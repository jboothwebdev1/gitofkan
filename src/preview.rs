use tui::{
    backend::Backend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    text::{Spans, Span},
    widgets::{Block, Borders, Paragraph, Wrap},
    Frame,
};

use crate::{app::App, components::greeting::draw_greeting};

pub fn draw_preview<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(100)])
        .margin(1)
        .split(area);

    let block = Block::default()
        .style(Style::default())
        .title("Preview")
        .borders(Borders::ALL);

    f.render_widget(block, area);

    let length = app.boards.items.len() - 1;

    match app.boards.selected.selected() {
        None => draw_greeting(f, app, chunks[0]),
        Some(x) => {
            if x != length {
                create_text(f, app, chunks[0])
            } else {
                draw_greeting(f, app, chunks[0])
            }
        }
    }
}

fn create_text<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    let selected = app.boards.selected.selected().unwrap() + 1;
    let mut text = vec![]; 
    if selected < app.boards.items.len() {
        for (index, i) in app.boards.items[selected].columns.iter().enumerate() {
            text.push(
                Spans::from(vec![
                    Span::styled(String::from(" * ") + &i.name, Style::default().fg(Color::Cyan))
                ])
            );
            for j in &app.boards.items[selected].columns[index].cards {
                text.push(
                    Spans::from(vec![
                        Span::raw("  "),
                        Span::raw("  "),
                        Span::styled(String::from(" - ") + &j.title, Style::default().fg(Color::Magenta))
                    ])
                );
            }
        }
    }
    f.render_widget(
        Paragraph::new(text)
            .alignment(Alignment::Left)
            .wrap(Wrap { trim: false }),
        area,
    )
}
