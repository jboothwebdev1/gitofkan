use crate::{
    app::App,
    cards::Card,
    columns::Column,
    components::popup::{Popup, PopupOption},
    task::Task,
};

pub fn save_task_from_popup(app: &mut App) {
    match app.popup_option {
        PopupOption::Column => create_new_column(app),
        PopupOption::Label => create_new_label(app),
        PopupOption::Task => create_new_card(app),
        PopupOption::SubTask => create_new_task(app),
        PopupOption::Assignee => add_assignee(app),
        _ => {}
    };

    app.popup = Popup::new(String::from(""));
    match app.popup_option {
        PopupOption::Task => app.popup_option = PopupOption::Label,
        PopupOption::Label => app.popup_option = PopupOption::SubTask,
        PopupOption::Assignee => app.popup_option = PopupOption::None,
        PopupOption::Column => app.popup_option = PopupOption::None,
        _ => app.popup_option = PopupOption::SubTask,
    };
}

fn create_new_card(app: &mut App) {
    let new_card = Card::new(String::from(&app.popup.input));
    let selected_card = app.boards.items[app.index].columns[app.selected_column]
        .cards
        .len();

    app.boards.items[app.index].columns[app.selected_column]
        .cards
        .push(new_card);
    app.boards.items[app.index].columns[app.selected_column].selected_card = selected_card;
    app.boards.items[app.index].columns[app.selected_column].card_count += 1;
}

fn create_new_column(app: &mut App) {
    let column = Column::new(app.popup.input.clone());
    let target_index = app.boards.items[app.index].columns.len() - 1;
    app.boards.items[app.index]
        .columns
        .insert(target_index, column)
}

fn create_new_label(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];
    selected_card
        .label
        .push(String::from(app.popup.input.clone()));
}

fn create_new_task(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];
    selected_card
        .task
        .push(Task::new(String::from(&app.popup.input)));
}

fn add_assignee(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];
    selected_card.assigned = String::from(&app.popup.input);
}
