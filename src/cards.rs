use serde::{Deserialize, Serialize};
use tui::layout::Layout;
use tui::text::Text;
use tui::{
    backend::Backend,
    layout::{Alignment, Constraint, Direction, Rect},
    style::{Color, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, List, ListItem, Paragraph, Wrap},
    Frame,
};

use crate::{app::App, task::Task};

/// Cards the respresentation of the tasks to be displayed in the columms
/// They are also stored in the Columns object in the db.
///
/// # Fields
/// - `title` - name of the tasks
/// - `tabs` - a vec this will probable just be a Vec<String> by might need to make a struct.
/// - `task` - a Vec<Task>
/// - `assigned` - the person that the task is assigned to.  
///
#[derive(Serialize, Deserialize, Clone)]
pub struct Card {
    pub title: String,
    pub label: Vec<String>,
    pub task: Vec<Task>,
    pub assigned: String,
    pub selected_subtask: usize,
}

impl Card {
    pub fn new(title: String) -> Self {
        Card {
            title,
            label: vec![],
            task: vec![],
            assigned: String::from("None"),
            selected_subtask: 0,
        }
    }
}

pub fn next_subtask(app: &mut App) {
    let board_index = app.index;
    let column_index = app.selected_column;
    let card_index = app.boards.items[board_index].columns[column_index].selected_card;
    let card = &mut app.boards.items[board_index].columns[column_index].cards[card_index];

    if card.task.len() > 0 {
        if card.selected_subtask + 1 > card.task.len() - 1 {
            card.selected_subtask = 0
        } else {
            card.selected_subtask += 1
        }
    }
}

pub fn prev_subtask(app: &mut App) {
    let board_index = app.index;
    let column_index = app.selected_column;
    let card_index = app.boards.items[board_index].columns[column_index].selected_card;
    let card = &mut app.boards.items[board_index].columns[column_index].cards[card_index];

    if card.task.len() > 0 {
        if card.selected_subtask > 0 {
            card.selected_subtask -= 1
        } else {
            card.selected_subtask = card.task.len() - 1
        }
    }
}

pub fn toggle_subtask_complete(app: &mut App) {
    let board_index = app.index;
    let column_index = app.selected_column;

    if app.boards.items[board_index].columns[column_index].cards
        [app.boards.items[board_index].columns[column_index].selected_card]
        .task
        .len()
        > 0
    {
        let card_index = app.boards.items[board_index].columns[column_index].selected_card;
        let card = &mut app.boards.items[board_index].columns[column_index].cards[card_index];

        card.task[card.selected_subtask].completed = !card.task[card.selected_subtask].completed
    }
}

pub fn draw_card<B>(f: &mut Frame<B>, area: Rect, card: &Card, selected: bool)
where
    B: Backend,
{
    let inner_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(85), Constraint::Percentage(10)].as_ref())
        .margin(1)
        .split(area);
    let list_area = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
                     Constraint::Percentage(20), Constraint::Percentage(80)
        ].as_ref())
        .split(inner_chunks[0]);

    let title = card.title.clone() + ": " + card.label.join(" ").as_ref();

    // draw the background of the card
   if selected {
        f.render_widget(
            Block::default()
                .borders(Borders::ALL)
                .title(title)
                .style(Style::default().fg(Color::Cyan)),
            area,
        );
    } else {
        f.render_widget(
            Block::default()
                .borders(Borders::ALL)
                .title(title)
                .style(Style::default().fg(Color::White)),
            area,
        );
    }

    if card.task.len() > 0 {
        let task_title = "Sub-tasks: ".to_string() + &(card.selected_subtask + 1).to_string() + "/" + &card.task.len().to_string();
        // draw the sub tasks
        f.render_widget(
            Paragraph::new(
            Text::styled(task_title , Style::default())
            ),list_area[0]);

        draw_subtask_list(f, &card, list_area[1], selected);
    }  

    // Draw the Assignee
    f.render_widget(
        Paragraph::new(Spans::from(vec![
            Span::styled("Assigned: ", Style::default()),
            Span::styled(&card.assigned, Style::default().fg(Color::White)),
        ]))
        .alignment(Alignment::Right)
        .wrap(Wrap { trim: true }),
        inner_chunks[1],
    )
}

fn draw_subtask_list<B>(f: &mut Frame<B>, card: &Card, area: Rect, selected: bool)
where
    B: Backend,
{
    let chunk = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(80)].as_ref())
        .margin(1)
        .split(area);

    // checking for out of index issues with the scrolling.
    let mut min_index = 0;
    let mut max_index = if card.task.len() > 0 && card.task.len() < 4 {
        card.task.len()
    } else if card.task.len() == 0 {
        0
    } else {
        4
    };
    let list_lenght: usize = if card.task.len() > 0 {
        card.task.len() - 1
    } else {
        0
    };

    (min_index, max_index) =
        check_for_scroll(card.selected_subtask, min_index, max_index, list_lenght);

    let mut items: Vec<ListItem> = vec![];

    // Starting the draw process for the list of sub-tasks.
    if max_index > 0 {
        for i in min_index..max_index {
            if i == card.selected_subtask && selected {
                let line = if card.task[i].completed {
                    let line = Spans::from(vec![
                        Span::styled(String::from("[x] - "), Style::default().fg(Color::Green)),
                        Span::styled(&card.task[i].title, Style::default().fg(Color::Green)),
                    ]);
                    line
                } else {
                    let line = Spans::from(vec![
                        Span::styled(
                            String::from("[ ] - "),
                            Style::default().fg(Color::LightMagenta),
                        ),
                        Span::styled(
                            &card.task[i].title,
                            Style::default().fg(Color::LightMagenta),
                        ),
                    ]);
                    line
                };
                items.push(ListItem::new(line).style(Style::default().bg(Color::DarkGray)));
            } else {
                let line = if card.task[i].completed {
                    let line = Spans::from(vec![
                        Span::styled(String::from("[x] - "), Style::default().fg(Color::Green)),
                        Span::styled(&card.task[i].title, Style::default().fg(Color::Green)),
                    ]);
                    line
                } else {
                    let line = Spans::from(vec![
                        Span::styled(
                            String::from("[ ] - "),
                            Style::default().fg(Color::LightMagenta),
                        ),
                        Span::styled(
                            &card.task[i].title,
                            Style::default().fg(Color::LightMagenta),
                        ),
                    ]);
                    line
                };
                items.push(ListItem::new(line).style(Style::default()));
            };
        }
    } else if card.task.len() == 1 {
        if selected {
            let line = if card.task[0].completed {
                let line = Spans::from(vec![
                    Span::styled(String::from("[x] - "), Style::default().fg(Color::Green)),
                    Span::styled(&card.task[0].title, Style::default().fg(Color::Green)),
                ]);
                line
            } else {
                let line = Spans::from(vec![
                    Span::styled(
                        String::from("[ ] - "),
                        Style::default().fg(Color::LightMagenta),
                    ),
                    Span::styled(
                        &card.task[0].title,
                        Style::default().fg(Color::LightMagenta),
                    ),
                ]);
                line
            };
            items.push(ListItem::new(line).style(Style::default().bg(Color::DarkGray)));
        } else {
            let line = if card.task[0].completed {
                let line = Spans::from(vec![
                    Span::styled(String::from("[x] - "), Style::default().fg(Color::Green)),
                    Span::styled(&card.task[0].title, Style::default().fg(Color::Green)),
                ]);
                line
            } else {
                let line = Spans::from(vec![
                    Span::styled(
                        String::from("[ ] - "),
                        Style::default().fg(Color::LightMagenta),
                    ),
                    Span::styled(
                        &card.task[0].title,
                        Style::default().fg(Color::LightMagenta),
                    ),
                ]);
                line
            };
            items.push(ListItem::new(line).style(Style::default()));
        };
    };

    let items = List::new(items);

    f.render_widget(items, chunk[0]);
}

fn check_for_scroll(
    selected_subtask: usize,
    min_index: usize,
    max_index: usize,
    list_lenght: usize,
) -> (usize, usize) {
    let new_min: usize;
    let new_max: usize;

    if selected_subtask == 0 {
        new_min = 0;
        new_max = if max_index > list_lenght {
            list_lenght + 1
        } else {
            max_index
        };
    } else if selected_subtask >= max_index {
        if max_index + 4 > list_lenght {
            new_min = if list_lenght > 3 { list_lenght - 3 } else { 0 };
            new_max = list_lenght + 1;
        } else {
            new_min = max_index;
            new_max = max_index + 4;
        }
    } else {
        new_min = min_index;
        new_max = max_index;
    }
    (new_min, new_max)
}
