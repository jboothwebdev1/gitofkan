use crate::{
    app::check_for_popups,
    columns::draw_column,
    command_map::{
        draw_board_page_map, draw_detele_cmd_map, draw_home_cmd_map, draw_popup_cmd_map,
    },
    components::{
        delete::{check_for_delete_popup},
        edit::{check_for_edit_popup},
        popup::{check_for_popup, draw_error_popup},
    },
    list::draw_boards_list,
    preview::draw_preview,
    utils,
};

use super::App;
use tui::{
    backend::Backend,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, Tabs},
    Frame,
};

/// Contains the root logic for drawing the UI.
/// The main branches from here are the draw_home and draw_board.
///
/// # Tabs
/// - Are the text on the top denoting which page is currently in view based off the tab index.
///
/// # Main body
///
/// The if statement on the bottom is current updating the display based off of the index stored
/// in the app struct.
///
pub fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    let size = f.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(3)
        .constraints(
            [
                Constraint::Percentage(5),
                Constraint::Percentage(90),
                Constraint::Percentage(5),
            ]
            .as_ref(),
        )
        .split(size);

    let titles: Vec<Spans> = app
        .boards
        .items
        .iter()
        .map(|board| Spans::from(vec![Span::styled(&board.title, Style::default())]))
        .collect();

    let tabs = Tabs::new(titles)
        .block(Block::default())
        .select(app.index)
        .style(Style::default().fg(Color::White))
        .highlight_style(
            Style::default()
                .add_modifier(Modifier::BOLD)
                .fg(Color::Cyan),
        );

    f.render_widget(tabs, chunks[0]);

    // Display the main body of the app.
    if app.index > 0 {
        draw_board(f, app, chunks[1]);
    } else {
        draw_home(f, app, chunks[1]);
    }

    // check for any needed popups.
    check_for_popup(f, app, centered_rect(20, 10, size));
    check_for_delete_popup(f, app, centered_rect(20, 10, size));
    check_for_edit_popup(f, app, centered_rect(20, 10, size));

    // Draw the keymaps at the bottom of the screen.
    match check_for_popups(&app) {
        Some(utils::BOARD) => draw_board_page_map(f, chunks[2]),
        Some(utils::CREATE_POPUP) => draw_popup_cmd_map(f, chunks[2]),
        Some(utils::DELETE_POPUP) => draw_detele_cmd_map(f, chunks[2]),
        Some(utils::EDIT_POPUP) => draw_popup_cmd_map(f, chunks[2]),
        Some(utils::CREATE_BOARD_POPUP) => draw_popup_cmd_map(f, chunks[2]),
        _ => draw_home_cmd_map(f, chunks[2]),
    }

    // Draw and error popup if there was a non-critical error thrown
    if app.request_error {
        draw_error_popup(f, app, centered_rect(10, 5, chunks[2]));
    }
}

/// Draw the selected board based on the current index in the app struct
///
fn draw_board<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    let columns = &app.boards.items[app.index].columns;

    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage(25),
                Constraint::Percentage(25),
                Constraint::Percentage(25),
                Constraint::Percentage(25),
            ]
            .as_ref(),
        )
        .split(area);
    // Setting the scrolling for the columns
    let mut min_index = 0;
    let mut max_index = if columns.len() > 0 && columns.len() < 4 {
        columns.len() 
    } else if columns.len() == 0 {
        0
    } else {
        4
    };

    let total_columns = if columns.len() > 0 {
        columns.len() - 1
    } else {
        0
    };

    (min_index, max_index) = check_for_column_scroll(app.selected_column, min_index, max_index, total_columns);

    let mut position = 0;
    for i in min_index..max_index {
        if app.selected_column == i {
            draw_column(f, chunks[position], true, &columns[i]);
        } else {
            draw_column(f, chunks[position], false, &columns[i]);
        }
        position += 1;
    }
}

fn check_for_column_scroll(
    selected_column: usize,
    min_index: usize,
    max_index: usize,
    list_length: usize
    ) -> (usize, usize){

    let new_min: usize;
    let new_max: usize;
    if selected_column == 0 {
        new_min = 0;
        new_max = if max_index > list_length {
            list_length + 1
        } else  {
            max_index
        };
    } else if selected_column >= max_index {
        if max_index + 4 > list_length {
            new_min = if list_length > 3 { list_length - 3} else { 0 };
            new_max = list_length + 1;
        } else {
            new_min = max_index;
            new_max = max_index + 4;
        }
    } else {
        new_max = max_index;
        new_min = min_index;
    }
    (new_min, new_max)
}

/// Draws the home page for the app.
///
/// # Currently
/// The board will be displayed in the tab bar and in the side bar labeled `Boards`
///     - This will be a list of the boards that will be selectable
///
/// The preview sections will hold basic instructions and show a preview if a Board is selected in
/// the side bard.
///
fn draw_home<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(20), Constraint::Percentage(80)].as_ref())
        .split(area);

    let project_block = Block::default()
        .style(Style::default())
        .title("Boards")
        .borders(Borders::ALL);
    f.render_widget(project_block, chunks[0]);

    let inner_area = Rect {
        x: chunks[0].x + 2,
        y: chunks[0].y + 2,
        width: chunks[0].width - 3,
        height: chunks[0].height - 2,
    };

    draw_boards_list(f, app, inner_area);

    draw_preview(f, app, chunks[1])
}

fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - percent_y) / 2),
                Constraint::Percentage(percent_y),
                Constraint::Percentage((100 - percent_y) / 2),
            ]
            .as_ref(),
        )
        .split(r);
    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - percent_x) / 2),
                Constraint::Percentage(percent_x),
                Constraint::Percentage((100 - percent_x) / 2),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}
