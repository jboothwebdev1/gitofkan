use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct Task {
    pub title: String,
    pub completed: bool,
}

impl Task {
    pub fn new(title: String) -> Self {
        Task {
            title,
            completed: false,
        }
    }
}
