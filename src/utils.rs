pub const BOARD: &str = "board";
pub const CREATE_POPUP: &str = "create";
pub const CREATE_BOARD_POPUP: &str = "create board";
pub const EDIT_POPUP: &str = "edit";
pub const DELETE_POPUP: &str = "delete";
