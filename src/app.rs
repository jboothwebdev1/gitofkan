use std::{
    fs, io,
    time::{Duration, Instant}, path::PathBuf,
};

use crossterm::event::{self, Event};
use serde::{Deserialize, Serialize};
use tui::{backend::Backend, Terminal};

use crate::{
    columns::Column,
    components::{
        delete::DeleteOption,
        edit::EditOption,
        popup::{Popup, PopupOption},
    },
    db::{get_boards, save_boards},
    keys::{
        board_keys::{
            match_board_event, match_card_popup_event, match_delete_event, match_edit_event,
        },
        home_keys::{match_board_popup_event, match_home_event},
    },
    list::SelectableList,
    ui::ui,
    utils,
};

/// Boards hold the columns. One board associates with one project.
///
/// # fields
/// - `title` - will be used for the tab bar and projects section.
/// - `columns` - The colums that are assign to this board that will hold the tasks.
///
/// # TODO:
/// - [ ] Eventually this will probably move out to it's own module.
/// - [ ] will probably need a new field for holding the current selection.
/// - [ ] Need to create a way to create boards
#[derive(Serialize, Deserialize, Clone)]
pub struct Board {
    pub title: String,
    pub columns: Vec<Column>,
}

/// The main struct of the application will be tracking most of the state at this moment.
///
/// # fields
/// - `boards` - holds all the current projects as boards. currently hard coded in the new
/// function.
/// - `index` - The current page index of the application. Home page and board will be linked to
/// this and used to change the views.
/// - `popup` - The structure that holds the title and the input of the popup
/// - `popup_option` - An enum of the type of popup to display Defaults to None.
///
pub struct App {
    pub boards: SelectableList<Board>,
    pub selected_column: usize,
    pub index: usize,
    pub delete_option: DeleteOption,
    pub edit_option: EditOption,
    pub popup_option: PopupOption,
    pub popup: Popup,
    pub request_shutdown: bool,
    pub request_error: bool,
}

impl App {
    pub fn new(db_path: &mut str) -> App {
        let mut shutdown = false;
        let boards = match get_boards(db_path) {
            Ok(boards) => boards,
            Err(_err) => {

                fs::create_dir(&db_path).unwrap_or_else(|err| {
                    eprintln!("{}", err)
                });
                match fs::File::create(db_path.to_owned() + "/kanr_db.json"){
                    Ok(_) => {}
                    Err(_) => {
                        eprint!("Failed to create db.json, check routing permissions");
                        shutdown = true;
                    }
                }
                vec![Board {
                    title: "Dashboard".to_string(),
                    columns: vec![],
                }]
            }
        };

        App {
            index: 0,
            delete_option: DeleteOption::None,
            popup_option: PopupOption::None,
            edit_option: EditOption::None,
            popup: Popup::new(String::new()),
            request_shutdown: shutdown,
            boards: SelectableList::new(boards),
            selected_column: 0,
            request_error: false,
        }
    }

    pub fn next(&mut self) {
        if (self.index + 1) > (self.boards.items.len() - 1) {
            self.index = 0;
        } else {
            self.index += 1;
        }
    }

    pub fn previous(&mut self) {
        if self.index > 0 {
            self.index -= 1;
        } else {
            self.index = self.boards.items.len() - 1;
        }
    }

    pub fn select_next_column(&mut self) {
        if (self.selected_column + 1) > (self.boards.items[self.index].columns.len() - 1) {
            self.selected_column = 0;
        } else {
            self.selected_column += 1;
        }
    }

    pub fn select_prev_column(&mut self) {
        if self.selected_column > 0 {
            self.selected_column -= 1;
        } else {
            self.selected_column = self.boards.items[self.index].columns.len() - 1;
        }
    }

    pub fn clear_popup(&mut self) {
        self.popup = Popup::new(String::new());
    }

    pub fn exit(&mut self) { 
        match save_boards(self) {
            Ok(_) => self.request_shutdown = true,
            Err(_) => self.request_error = true
        };
    }
}

/// Main process of the app responsible for the highest call to draw the UI, controls timing, and sets the current
/// keymaps.        
pub fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
    tick_rate: Duration,
) -> io::Result<()> {
    loop {
        let mut last_tick = Instant::now();
// draw the terminal
        terminal.draw(|f| ui(f, &mut app))?;

        // add the events
        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));

        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                // Set the right keybindings for the current operation.
                match check_for_popups(&app) {
                    Some(utils::BOARD) => match_board_event(key, &mut app),
                    Some(utils::CREATE_POPUP) => match_card_popup_event(key, &mut app),
                    Some(utils::DELETE_POPUP) => match_delete_event(key, &mut app),
                    Some(utils::EDIT_POPUP) => match_edit_event(key, &mut app),
                    Some(utils::CREATE_BOARD_POPUP) => match_board_popup_event(key, &mut app),
                    _ => match_home_event(key, &mut app),
                }
            }
        }
        // Exits the app
        if app.request_shutdown {
            return Ok(());
        }

        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now();
            app.request_error = false;
        }
    }
}

pub fn check_for_popups(app: &App) -> Option<&'static str> {
    let mut outcome = Some(utils::BOARD);
    if app.index == 0 {
        match app.popup_option {
            PopupOption::Board => outcome = Some(utils::CREATE_BOARD_POPUP),
            _ => outcome = None,
        }
    } else {
        match app.popup_option {
            PopupOption::None => {}
            _ => outcome = Some(utils::CREATE_POPUP),
        }
        match app.edit_option {
            EditOption::None => {}
            _ => outcome = Some(utils::EDIT_POPUP),
        }
        match app.delete_option {
            DeleteOption::None => {}
            _ => outcome = Some(utils::DELETE_POPUP),
        }
    }
    outcome
}
