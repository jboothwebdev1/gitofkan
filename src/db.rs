use std::{error::Error, fs};

use crate::app::{App, Board};

pub fn get_boards(db_path: &mut str) -> Result<Vec<Board>, Box<dyn Error>> {
    let full_path = db_path.to_owned() + "/kanr_db.json";
    let db_contents = fs::read_to_string(full_path)?;
    let parsed = serde_json::from_str(&db_contents)?;
    Ok(parsed)
}

pub fn save_boards(app: &App) -> Result<(), Box<dyn Error>> {
    let root = match home::home_dir() {
            Some(path) => path,
            None => {
                eprintln!("Could not find home directory");
                std::process::exit(1);
            }
    };
    let db_path = root
        .join("data/kanr_db.json")
        .to_str()
        .unwrap()
        .to_owned();
    let parsed = serde_json::to_vec(&app.boards.items)?;
    fs::write(db_path, parsed)?;
    Ok(())
}
