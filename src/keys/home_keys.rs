use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};

use crate::{
    app::App,
    components::popup::{save_board_from_popup, PopupOption},
};
/// Keymap only to be used on the dashboard page.
pub fn match_home_event(key: KeyEvent, app: &mut App) {
    match key {
        KeyEvent {
            modifiers: KeyModifiers::CONTROL,
            code: KeyCode::Char('c'),
            ..
        } => app.exit(),
        KeyEvent {
            code: KeyCode::Tab, ..
        } => app.next(),
        KeyEvent {
            code: KeyCode::BackTab,
            ..
        } => app.previous(),
        KeyEvent {
            code: KeyCode::Char('b'),
            ..
        } => app.popup_option = PopupOption::Board,
        KeyEvent {
            code: KeyCode::Down,
            ..
        } => app.boards.next(),
        KeyEvent {
            code: KeyCode::Up, ..
        } => app.boards.previous(),
        _ => {}
    }
}

pub fn match_board_popup_event(key: KeyEvent, app: &mut App) {
    match key {
        KeyEvent {
            code: KeyCode::Esc, ..
        } => app.popup_option = PopupOption::None,
        KeyEvent {
            code: KeyCode::Char(c),
            ..
        } => app.popup.input.push(c),
        KeyEvent {
            code: KeyCode::Backspace,
            ..
        } => {
            app.popup.input.pop();
        }
        KeyEvent {
            code: KeyCode::Enter,
            ..
        } => save_board_from_popup(app),
        _ => {}
    }
}
