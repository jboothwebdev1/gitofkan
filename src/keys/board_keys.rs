use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};

use crate::{
    app::App,
    cards::{next_subtask, prev_subtask, toggle_subtask_complete},
    columns::{move_card_left, move_card_right},
    components::{
        delete::{delete_from_popup, DeleteOption},
        edit::{save_edit_from_popup, EditOption},
        popup::PopupOption,
    },
    saving::save_task_from_popup,
};

/// Keymap to be used for every board other than the dashboard.
pub fn match_board_event(key: KeyEvent, app: &mut App) {
    match key {
        KeyEvent {
            modifiers: KeyModifiers::CONTROL,
            code: KeyCode::Char('c'),
            ..
        } => app.exit(),
        KeyEvent {
            code: KeyCode::Char('l'),
            modifiers: KeyModifiers::CONTROL,
            ..
        } => app.delete_option = DeleteOption::Label,
        KeyEvent {
            code: KeyCode::Char('u'),
            modifiers: KeyModifiers::CONTROL,
            ..
        } => app.delete_option = DeleteOption::Column,
        KeyEvent {
            code: KeyCode::Char(';'),
            ..
        } => app.edit_option = EditOption::Column,

        KeyEvent {
            code: KeyCode::Char('l'),
            ..
        } => app.select_next_column(),

        KeyEvent {
            code: KeyCode::Char('h'),
            ..
        } => app.select_prev_column(),
        KeyEvent {
            code: KeyCode::Char('j'),
            ..
        } => app.boards.items[app.index].columns[app.selected_column].next_card(),
        KeyEvent {
            code: KeyCode::Char('k'),
            ..
        } => app.boards.items[app.index].columns[app.selected_column].prev_card(),
        KeyEvent {
            modifiers: KeyModifiers::CONTROL,
            code: KeyCode::Char('p'),
            ..
        } => move_card_left(app),
        KeyEvent {
            modifiers: KeyModifiers::CONTROL,
            code: KeyCode::Char('n'),
            ..
        } => move_card_right(app),

        KeyEvent {
            code: KeyCode::Up, ..
        } => {
            if app.boards.items[app.index].columns[app.selected_column]
                .cards
                .len()
                > 0
            {
                prev_subtask(app);
            }
        }
        KeyEvent {
            code: KeyCode::Down,
            ..
        } => {
            if app.boards.items[app.index].columns[app.selected_column]
                .cards
                .len()
                > 0
            {
                next_subtask(app);
            }
        }
        KeyEvent {
            code: KeyCode::Right,
            ..
        } => {
            if app.boards.items[app.index].columns[app.selected_column]
                .cards
                .len()
                > 0
            {
                toggle_subtask_complete(app);
            }
        }
        KeyEvent {
            code: KeyCode::Char('/'),
            ..
        } => app.edit_option = EditOption::Assignee,
        KeyEvent {
            code: KeyCode::Char('A'),
            ..
        } => app.popup_option = PopupOption::Assignee,
        KeyEvent {
            code: KeyCode::Char('a'),
            modifiers: KeyModifiers::CONTROL,
            ..
        } => app.delete_option = DeleteOption::Assignee,
        KeyEvent {
            code: KeyCode::Char('m'),
            ..
        } => app.edit_option = EditOption::Board,

        KeyEvent {
            code: KeyCode::Char('b'),
            modifiers: KeyModifiers::CONTROL,
            ..
        } => app.delete_option = DeleteOption::Board,
        KeyEvent {
            code: KeyCode::Char('C'),
            modifiers: KeyModifiers::CONTROL,
            ..
        } => app.delete_option = DeleteOption::Column,
        KeyEvent {
            code: KeyCode::Char('C'),
            ..
        } => app.popup_option = PopupOption::Column,
        KeyEvent {
            code: KeyCode::Char(','),
            ..
        } => app.edit_option = EditOption::Subtask,
        KeyEvent {
            code: KeyCode::Char('s'),
            modifiers: KeyModifiers::CONTROL,
            ..
        } => app.delete_option = DeleteOption::SubTask,
        KeyEvent {
            code: KeyCode::Char('S'),
            ..
        } => app.popup_option = PopupOption::SubTask,
        KeyEvent {
            code: KeyCode::Tab, ..
        } => app.next(),
        KeyEvent {
            code: KeyCode::BackTab,
            ..
        } => app.previous(),
        KeyEvent {
            code: KeyCode::Char('t'),
            modifiers: KeyModifiers::CONTROL,
            ..
        } => app.delete_option = DeleteOption::Task,
        KeyEvent {
            code: KeyCode::Char('.'),
            ..
        } => app.edit_option = EditOption::Task,
        KeyEvent {
            code: KeyCode::Char('T'),
            ..
        } => app.popup_option = PopupOption::Task,
        KeyEvent {
            code: KeyCode::Char('X'),
            ..
        } => app.delete_option = DeleteOption::ClearColumn,
        _ => {}
    }
}

/// Keymap to be on the popup that creates the cards
pub fn match_card_popup_event(key: KeyEvent, app: &mut App) {
    match key {
        KeyEvent {
            code: KeyCode::Esc, ..
        } => app.popup_option = PopupOption::None,
        KeyEvent {
            code: KeyCode::Char(c),
            ..
        } => app.popup.input.push(c),
        KeyEvent {
            code: KeyCode::Backspace,
            ..
        } => {
            app.popup.input.pop();
        }
        KeyEvent {
            code: KeyCode::Enter,
            ..
        } => save_task_from_popup(app),
        _ => {}
    }
}

pub fn match_delete_event(key: KeyEvent, app: &mut App) {
    match key {
        KeyEvent {
            code: KeyCode::Esc, ..
        } => app.delete_option = DeleteOption::None,
        KeyEvent {
            code: KeyCode::Enter,
            ..
        } => delete_from_popup(app),
        _ => {}
    }
}

pub fn match_edit_event(key: KeyEvent, app: &mut App) {
    match key {
        KeyEvent {
            code: KeyCode::Esc, ..
        } => app.edit_option = EditOption::None,
        KeyEvent {
            code: KeyCode::Char(c),
            ..
        } => app.popup.input.push(c),
        KeyEvent {
            code: KeyCode::Backspace,
            ..
        } => {
            app.popup.input.pop();
        }
        KeyEvent {
            code: KeyCode::Enter,
            ..
        } => save_edit_from_popup(app),
        _ => {}
    }
}
