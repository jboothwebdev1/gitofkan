mod app;
mod cards;
mod columns;
mod command_map;
mod components;
mod db;
mod keys;
mod list;
mod preview;
mod saving;
mod task;
mod ui;
mod utils;

use crossterm::{
    event::{DisableMouseCapture, EnableMouseCapture},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use std::{error::Error, io, time::Duration};
use tui::{backend::CrosstermBackend, Terminal};

use app::{run_app, App};


fn main() -> Result<(), Box<dyn Error>> {
    // setup terminal
    enable_raw_mode()?;
    let root = match home::home_dir() {
        Some(path) => path,
        None => {
            eprintln!("Could not find home directory");
            std::process::exit(1);
        }
    };
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let mut dir_path = root.to_str().unwrap().to_owned() + "/data";
    let tick_rate = Duration::from_millis(200);
    let app = App::new(&mut dir_path);
    let res = run_app(&mut terminal, app, tick_rate);
    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture,
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err)
    }

    Ok(())
}
