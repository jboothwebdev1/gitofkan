use serde::{Deserialize, Serialize};
use tui::{
    backend::Backend,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    widgets::{Block, Borders},
    Frame,
};

use crate::{
    app::App,
    cards::{draw_card, Card},
};

/// Column hold tasks and are stored in boards.
///
/// # fields
/// - `name` - will be used for the label on the outer block of the column.
/// - `card_count` - will show how many tasks are currently in the column.
/// - `selected_card` - The currently seleected card in this column.
/// - `cards` The list of card structs that belong to this column.
///
#[derive(Serialize, Deserialize, Clone)]
pub struct Column {
    pub name: String,
    pub card_count: u32,
    pub selected_card: usize,
    pub cards: Vec<Card>,
}

impl Column {
    pub fn new(name: String) -> Self {
        Column {
            name,
            card_count: 0,
            selected_card: 0,
            cards: vec![],
        }
    }

    pub fn default() -> Vec<Column> {
        vec![
            Column {
                name: "Open".to_string(),
                card_count: 0,
                selected_card: 0,
                cards: vec![],
            },
            Column {
                name: "Closed".to_string(),
                card_count: 0,
                selected_card: 0,
                cards: vec![],
            },
        ]
    }

    pub fn next_card(&mut self) {
        if self.cards.len() > 0 {
            if self.selected_card + 1 > self.cards.len() - 1 {
                self.selected_card = 0
            } else {
                self.selected_card += 1
            }
        }
    }

    pub fn prev_card(&mut self) {
        if self.cards.len() > 0 {
            if self.selected_card > 0 {
                self.selected_card -= 1
            } else {
                self.selected_card = self.cards.len() - 1
            }
        }
    }
    pub fn clear_column(&mut self) {
        self.selected_card = 0;
        self.cards = vec![];
        self.card_count = 0;
    }
}

pub fn move_card_right(app: &mut App) {
    let current_board = &mut app.boards.items[app.index];
    let selected_card = current_board.columns[app.selected_column]
        .selected_card
        .clone();

    if app.selected_column + 1 <= current_board.columns.len() - 1
        && current_board.columns[app.selected_column].cards.len() > 0
    {
        // pop the card.
        let card = current_board.columns[app.selected_column]
            .cards
            .remove(selected_card);
        // adjust the counter
        current_board.columns[app.selected_column].card_count -= 1;
        current_board.columns[app.selected_column].selected_card = 0;
        // add and adjust the new column
        current_board.columns[app.selected_column + 1]
            .cards
            .push(card.to_owned());
        current_board.columns[app.selected_column + 1].card_count += 1;
        
        // have the selection follow the card.
        app.selected_column += 1;
        current_board.columns[app.selected_column].selected_card = current_board.columns[app.selected_column].cards.len() - 1;
    }
}

pub fn move_card_left(app: &mut App) {
    let current_board = &mut app.boards.items[app.index];
    let selected_card = current_board.columns[app.selected_column]
        .selected_card
        .clone();
    if app.selected_column > 0 && current_board.columns[app.selected_column].cards.len() > 0 {
        let card = current_board.columns[app.selected_column]
            .cards
            .remove(selected_card);
        //adjust the counter
        current_board.columns[app.selected_column].card_count -= 1;
        current_board.columns[app.selected_column].selected_card = 0;
        current_board.columns[app.selected_column - 1]
            .cards
            .push(card.to_owned());
        current_board.columns[app.selected_column - 1].card_count += 1;

        // have the selection follow the card.
        app.selected_column -= 1;
        current_board.columns[app.selected_column].selected_card = current_board.columns[app.selected_column].cards.len() - 1;
    }
}

pub fn draw_column<B>(f: &mut Frame<B>, area: Rect, selected: bool, column: &Column)
where
    B: Backend,
{
    let title = column.name.clone() + &String::from(" : Tasks ") + &column.card_count.to_string();

    if selected {
        f.render_widget(
            Block::default()
                .title(title)
                .borders(Borders::ALL)
                .style(Style::default().fg(Color::Magenta)),
            area,
        )
    } else {
        f.render_widget(
            Block::default()
                .title(column.name.clone())
                .borders(Borders::ALL)
                .style(Style::default().fg(Color::White)),
            area,
        )
    }

    // Set the area for each card
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage(23),
                Constraint::Percentage(23),
                Constraint::Percentage(23),
                Constraint::Percentage(23),
            ]
            .as_ref(),
        )
        .margin(1)
        .split(area);
    // Here we draw the cards inside of the column
    let total_cards = if column.cards.len() > 0 {
        column.cards.len() - 1
    } else {
        0
    };

    let mut min_index = 0;
    let mut max_index = if column.cards.len() > 0 && column.cards.len() < 4 {
        column.cards.len()
    } else if column.cards.len() == 0 {
        0
    } else {
        4
    };

    (min_index, max_index) =
        check_for_scroll(column.selected_card, min_index, max_index, total_cards);

    let mut position = 0;
    if max_index > 0 {
        for i in min_index..max_index {
            if selected && column.selected_card == i {
                draw_card(f, chunks[position], &column.cards[i], true);
            } else {
                draw_card(f, chunks[position], &column.cards[i], false);
            }
            position += 1;
        }
    } else if column.cards.len() == 1 {
        if selected {
            draw_card(f, chunks[0], &column.cards[0], true);
        } else {
            draw_card(f, chunks[0], &column.cards[0], false);
        }
    }
}

/// checks the selected_card if it is equal to the max_index. If so adjust the min and max indexes to scroll the list.
fn check_for_scroll(
    selected_card: usize,
    min_index: usize,
    max_index: usize,
    list_length: usize,
) -> (usize, usize) {
    let new_min: usize;
    let new_max: usize;
    if selected_card == 0 {
        new_min = 0;
        new_max = if max_index > list_length {
            list_length + 1
        } else {
            max_index
        };
    } else if selected_card >= max_index {
        if max_index + 4 > list_length {
            new_min = if list_length > 3 { list_length - 3 } else { 0 };
            new_max = list_length + 1;
        } else {
            new_min = max_index;
            new_max = max_index + 4;
        }
    } else {
        new_max = max_index;
        new_min = min_index;
    }

    (new_min, new_max)
}
