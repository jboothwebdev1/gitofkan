use tui::{
    backend::Backend,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, List, ListItem, ListState},
    Frame,
};

use crate::app::App;

pub struct SelectableList<T> {
    pub selected: ListState,
    pub items: Vec<T>,
}

impl<T> SelectableList<T> {
    pub fn new(items: Vec<T>) -> Self {
        SelectableList {
            selected: ListState::default(),
            items,
        }
    }

    pub fn next(&mut self) {
        let i = match self.selected.selected() {
            Some(i) => {
                if i >= self.items.len() - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.selected.select(Some(i));
    }

    pub fn previous(&mut self) {
        let i = match self.selected.selected() {
            Some(i) => {
                if i == 0 {
                    self.items.len() - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.selected.select(Some(i));
    }
}

pub fn draw_boards_list<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(100)].as_ref())
        .split(area);

    let mut items: Vec<ListItem> = app
        .boards
        .items
        .iter()
        .map(|i| {
            let line = Spans::from(Span::styled(
                String::from(" - ") + &i.title,
                Style::default(),
            ));
            ListItem::new(line).style(Style::default())
        })
        .collect();

    // remove the dashboard.
    items.remove(0);

    let items = List::new(items)
        .block(Block::default())
        .highlight_style(
            Style::default()
                .fg(Color::LightCyan)
                .add_modifier(Modifier::ITALIC),
        )
        .highlight_symbol("> ");

    f.render_stateful_widget(items, layout[0], &mut app.boards.selected);
}
