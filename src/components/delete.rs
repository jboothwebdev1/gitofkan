use tui::{
    backend::Backend,
    layout::{Constraint, Layout, Rect},
    style::{Color, Style},
    text::Span,
    widgets::{Block, Borders, Clear, Paragraph},
    Frame,
};

use crate::{app::App};

pub enum DeleteOption {
    Assignee,
    Board,
    Column,
    Task,
    Label,
    SubTask,
    ClearColumn,
    None,
}

pub fn select_delete_confirm<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    match app.delete_option {
        DeleteOption::Assignee => draw_delete_confirm(f, area, "Delete Assignee?".to_string()),
        DeleteOption::Board => draw_delete_confirm(f, area, "Delete Board?".to_string()),
        DeleteOption::Label => draw_delete_confirm(f, area, "Delete Label?".to_string()),
        DeleteOption::Column => draw_delete_confirm(f, area, "Delete Column?".to_string()),
        DeleteOption::SubTask => draw_delete_confirm(f, area, "Delete Sub-Task?".to_string()),
        DeleteOption::Task => draw_delete_confirm(f, area, "Delete Task?".to_string()),
        DeleteOption::ClearColumn => draw_delete_confirm(f, area, "Clear-Column?".to_string()),
        _ => {}
    }
}

pub fn check_for_delete_popup<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    match app.delete_option {
        DeleteOption::None => {}
        _ => select_delete_confirm(f, app, area),
    }
}

pub fn delete_from_popup(app: &mut App) {
    match app.delete_option {
        DeleteOption::Assignee => delete_assignee(app),
        DeleteOption::Board => delete_board(app),
        DeleteOption::Column => delete_column(app),
        DeleteOption::Label => delete_label(app),
        DeleteOption::SubTask => delete_subtask(app),
        DeleteOption::Task => delete_task(app),
        DeleteOption::ClearColumn => {
            app.boards.items[app.index].columns[app.selected_column].clear_column()
        }
        _ => {}
    }

    app.delete_option = DeleteOption::None;
}

fn delete_assignee(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];
    selected_card.assigned = String::from("None");
}

fn delete_board(app: &mut App) {
    if app.boards.items.len() > 1 && app.index != 0 {
        app.boards.items.remove(app.index);
        app.index = 0;
    }
}

fn delete_column(app: &mut App) {
    if app.boards.items[app.index].columns.len() > 0 {
        app.boards.items[app.index]
            .columns
            .remove(app.selected_column);
        app.selected_column -= 1;
    }
}

fn delete_label(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];

    selected_card.label.pop();
}

fn delete_subtask(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];

    if selected_card.task.len() > 0 {
        selected_card.task.remove(selected_card.selected_subtask);
        selected_card.selected_subtask = 0;
    }
}

fn delete_task(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    if selected_column.cards.len() > 0 {
        selected_column.cards.remove(selected_column.selected_card);
        selected_column.card_count -= 1;
        selected_column.selected_card = 0;
    }
}

fn draw_delete_confirm<B>(f: &mut Frame<B>, area: Rect, title: String)
where
    B: Backend,
{
    let block = Block::default()
        .style(Style::default().fg(Color::LightRed))
        .title(title)
        .borders(Borders::ALL);

    let paragraph = Paragraph::new(Span::raw("Confirm <Enter> : Cancel <Esc>"));
    let chunk = Layout::default()
        .constraints([Constraint::Percentage(100)].as_ref())
        .margin(1)
        .split(area);

    f.render_widget(Clear, area);
    f.render_widget(block, area);
    f.render_widget(paragraph, chunk[0])
}
