use tui::{
    backend::Backend,
    layout::Rect,
    style::{Color, Style},
    text::Text,
    widgets::{Block, Borders, Clear, Paragraph},
    Frame,
};

use crate::{
    app::{App, Board},
    columns::Column,
};

/// Enum for the type of popup that needs to be created.
pub enum PopupOption {
    Assignee,
    Board,
    Column,
    Task,
    Label,
    SubTask,
    None,
}

/// Struct for holding the title and the input of a popup.
///
/// # Fields
/// - `title` - The title of the popup displayed on the block.
/// - `input` - The user inputted data.
///
pub struct Popup {
    title: String,
    pub input: String,
}

impl Popup {
    pub fn new(title: String) -> Self {
        Popup {
            title,
            input: String::new(),
        }
    }

    pub fn get_title(&self) -> &str {
        &self.title
    }

    pub fn set_title(&mut self, title: &str) {
        self.title = title.to_string();
    }
}

/// Creates a new board based on the input in the popup.
pub fn save_board_from_popup(app: &mut App) {
    let new_board = Board {
        title: String::from(&app.popup.input),
        columns: Column::default(),
    };
    app.boards.items.push(new_board);
    app.popup = Popup::new(String::from(""));
    app.popup_option = PopupOption::None;
}

pub fn check_for_popup<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    match app.popup_option {
        PopupOption::None => {}
        PopupOption::Assignee => {
            app.popup.set_title("Assign Task");
            draw_popup(f, app, area);
        }
        PopupOption::Board => {
            app.popup.set_title("Create a new board");
            draw_popup(f, app, area);
        }
        PopupOption::Column => {
            app.popup.set_title("Add a new Column");
            draw_popup(f, app, area);
        }
        PopupOption::Label => {
            app.popup.set_title("Add Label");
            draw_popup(f, app, area);
        }
        PopupOption::SubTask => {
            app.popup.set_title("Add Sub-task");
            draw_popup(f, app, area);
        }
        PopupOption::Task => {
            app.popup.set_title("Add Task");
            draw_popup(f, app, area);
        }
    }
}

pub fn draw_popup<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    let block = Block::default()
        .style(Style::default().fg(Color::Cyan))
        .title(app.popup.get_title())
        .borders(Borders::ALL);

    let text = Text::from(app.popup.input.as_ref());
    let paragraph = Paragraph::new(text.clone()).style(Style::default().fg(Color::White));
    let inner_area = Rect {
        x: area.x + 2,
        y: area.y + 2,
        width: area.width - 3,
        height: area.height - 3,
    };

    f.render_widget(Clear, area);

    f.render_widget(block, area);
    f.render_widget(paragraph, inner_area);
    f.set_cursor(inner_area.x + text.width() as u16, inner_area.y)
}

pub fn draw_error_popup<B>(f: &mut Frame<B>, _app: &mut App, area: Rect)
where
    B: Backend,
{
    let block = Block::default()
        .style(Style::default().fg(Color::Cyan))
        .title("Error")
        .borders(Borders::ALL);

    let text = Text::from("Error Message");
    let paragraph = Paragraph::new(text.clone()).style(Style::default().fg(Color::Red));
    let inner_area = Rect {
        x: area.x + 2,
        y: area.y + 2,
        width: area.width - 3,
        height: area.height - 3,
    };

    f.render_widget(Clear, area);

    f.render_widget(block, area);
    f.render_widget(paragraph, inner_area);
    f.set_cursor(inner_area.x + text.width() as u16, inner_area.y)
}
