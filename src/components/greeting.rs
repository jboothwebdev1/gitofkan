use tui::{
    backend::Backend,
    layout::{Alignment, Constraint, Layout, Rect},
    style::{Color, Style},
    text::{Span, Text},
    widgets::{Paragraph, Wrap},
    Frame,
};

use crate::app::App;

pub fn draw_greeting<B>(f: &mut Frame<B>, _app: &mut App, area: Rect)
where
    B: Backend,
{
    let mut text = Text::from("");

    let chunks = Layout::default()
        .direction(tui::layout::Direction::Vertical)
        .constraints([Constraint::Percentage(5), Constraint::Percentage(95)].as_ref())
        .split(area);

    let title = Paragraph::new(Span::styled(
        "** WELCOME TO KANR **",
        Style::default().fg(Color::Magenta),
    ))
    .alignment(Alignment::Center);

    f.render_widget(title, chunks[0]);

    text.extend(Text::raw(" The CLI Kanban board. Currently all commands are through the key board and there are no mouse commands so play close attention to the keymaps at the bottom of the screen. To get started create you first board by hitting the 'b' button. \n\n \tAs you scroll through your boards you can create new boards and card/task and subtask creation is through UPPERCASE letters to delete the various items is done via the Control key and the corresponding lowercase letter. \n\n\t "));
    f.render_widget(
        Paragraph::new(text)
            .alignment(Alignment::Left)
            .wrap(Wrap { trim: true }),
        chunks[1],
    );
}
