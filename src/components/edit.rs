use tui::{
    backend::Backend,
    layout::{Rect},
    Frame,
};

use crate::{app::App, db::save_boards};

use super::popup::draw_popup;

pub enum EditOption {
    Assignee,
    Board,
    Column,
    Task,
    Subtask,
    None,
}

pub fn check_for_edit_popup<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    match app.edit_option {
        EditOption::None => {}
        _ => select_edit_popup(f, app, area),
    }
}

pub fn save_edit_from_popup(app: &mut App) {
    match app.edit_option {
        EditOption::Assignee => edit_assignee(app),
        EditOption::Board => edit_board(app),
        EditOption::Column => edit_column(app),
        EditOption::Task => edit_task(app),
        EditOption::Subtask => edit_subtask(app),
        EditOption::None => {}
    }

    match save_boards(&app) {
        Ok(_) => app.edit_option = EditOption::None,
        Err(_) => app.edit_option = EditOption::None,
    }
}

fn edit_assignee(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];
    selected_card.assigned = app.popup.input.clone();
    app.clear_popup();
}

fn edit_board(app: &mut App) {
    app.boards.items[app.index].title = app.popup.input.clone();
    app.clear_popup();
}

fn edit_column(app: &mut App) {
    app.boards.items[app.index].columns[app.selected_column].name = app.popup.input.clone();
    app.clear_popup();
}

fn edit_subtask(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];

    selected_card.task[selected_card.selected_subtask].title = app.popup.input.clone();
    app.clear_popup();
}

fn edit_task(app: &mut App) {
    let selected_column = &mut app.boards.items[app.index].columns[app.selected_column];
    let selected_card = &mut selected_column.cards[selected_column.selected_card];

    selected_card.title = app.popup.input.clone();
    app.clear_popup();
}

fn select_edit_popup<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    match app.edit_option {
        EditOption::Assignee => {
            app.popup.set_title("Edit Assignee");
            draw_popup(f, app, area)
        }
        EditOption::Board => {
            app.popup.set_title("Edit Board Title");
            draw_popup(f, app, area)
        }
        EditOption::Column => {
            app.popup.set_title("Edit Column Title");
            draw_popup(f, app, area);
        }
        EditOption::Task => {
            app.popup.set_title("Edit Task");
            draw_popup(f, app, area);
        }
        EditOption::Subtask => {
            app.popup.set_title("Edit Task");
            draw_popup(f, app, area);
        }
        EditOption::None => {}
    }
}
