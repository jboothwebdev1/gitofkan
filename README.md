# Kanr 
A CLI Kanban board / written in rust

![Alt text](./app_image.png)

## State of project 

Currently, in Pre-beta.


### Installation

The most recent version can be installed through `cargo` after cloning the main branch.

1. Clone the repo 
2. cd into folder
3.
```
cargo build --release
```
4. 
```
cargo install --path ./
```
You can download the binaries from the repository below. 


[ Releases ](https://gitlab.com/jboothwebdev1/releases)

After downloading it is recommended to move the directory and create an alias for easy access.

1. cd into the directory where you dowload the zip and extract it.
2. cd into the folder and you are going to move the release folder.
```
mv ./release ~/kanr
```
3. Add the path to kanr to you PATH in your shell rc file. 

## Concept 

For developers that love to stay in the terminal adding a project management tool to you terminal work flow. 

Once you install the tool you would create a board. 

That board is then linked to a project directory.

Currently, Thinking of ways to incorporate git with the board.

My original Idea I don't think would work. As such trying to think of the best way to handle that and make it viable.

## Plan of development

### Milestones 

- Note as building the kanban has been progressing I now see some added complexity in just the kanban board. As such I am updating the release stage targets. 

#### Pre-alpha 

- [x] Get the kanban working. 
	- [x] Finish the basic layout of the UI.
	- [x] Set up the keybindings 
	- [x] Set up the logic for the creation of boards, columns, and items.
- [x] Set up the storage for columns and task.
- [x] Implement at minimum deletion for all the Items.
- [x] Add ability to complete the sub-tasks. 

#### Pre-beta 

##### Changed

- [x] Full CRUD of the columns, tasks, and sub_tasks.
- [x] Fully fleshed out dashboard. 

### Beta-release 

- [ ] Incorporate the git features.

- Bugfixes for both the kanban side and the git,

### post-release

- Possible ideas for continuation.
    - Expand git functionality.
    - Terminal like git commands. 

## UI 

The UI is created with the TUI and crossterm crates. For more information see.

[TUI](https://docs.rs/tui/0.19.0/tui/index.html)

[crossterm](https://docs.rs/crossterm/latest/crossterm/index.html)


## Database 

Locally stored in the `/data/kanr_db.json` file in the project directory.

Every object in the file is a `Board`.

The `Columns` and `Cards/Tasks` will be stored in the respective fields in the board object.


